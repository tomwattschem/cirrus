#!/lustre/home/ec043/ball4935/opt/miniconda3/bin/python
import os
import argparse
import operator

omp_path = '/lustre/home/ec043/ball4935/opt/bin'
omp_lib_path = '/lustre/home/ec043/ball4935/opt/orca_4_1_2_linux_x86-64_shared_openmpi215'
orca_path = '/lustre/home/ec043/ball4935/opt/orca_4_1_2_linux_x86-64_shared_openmpi215/orca'
lib_path = '/lustre/home/ec043/ball4935/opt/lib'


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', help='.inp file(s) sumbit to the queue', nargs='+')

    return parser.parse_args()


def get_decomposition(file_names):

    file_names_and_n_cores = {}
    for inp_filename in [filename for filename in file_names if filename.endswith('.inp')]:
        inp_file_lines = open(inp_filename, 'r').readlines()
        for line in inp_file_lines:
            if '!' in line:
                keyword_line = line
                for item in keyword_line.split():
                    n_cores = 1
                    if item.startswith("PAL"):
                        try:
                            n_cores = int(item[3:])
                            file_names_and_n_cores[inp_filename] = n_cores
                            break
                        except ValueError:
                            exit('Could not extract the number of cores. Please look at your input file')
                    file_names_and_n_cores[inp_filename] = n_cores
            break

    # From the file names and the number of cores requested in each find the decomposition that affords totals
    # less than 36 cores, which will be executed exclusively on one node

    filenames_blocks_and_n_cores = []
    block_ncores = 0
    filenames_block = []

    # Sort the dictionary on the number of cores so we use *almost* the fewest number of notes
    for filename, n_cores in sorted(file_names_and_n_cores.items(), key=operator.itemgetter(1)):

        if block_ncores + n_cores > 36:         # Each node has 36 cores on Cirrus
            filenames_blocks_and_n_cores.append((filenames_block, block_ncores))
            filenames_block = []
            block_ncores = 0

        filenames_block.append(filename)
        block_ncores += n_cores

    if len(filenames_block) > 0:
        filenames_blocks_and_n_cores.append((filenames_block, block_ncores))

    return filenames_blocks_and_n_cores


def print_bash_script(filenames, n_cores, sh_filename):

    folder_names = list(set([os.path.dirname(filename) for filename in filenames]))

    with open(sh_filename, 'w') as bash_script:
        print('#!/bin/bash --login',
              f'#PBS -N {sh_filename[:-3]}',
              f'#PBS -l select=1:ncpus={n_cores}',
              '#PBS -l walltime=96:00:00',
              '#PBS -l place=scatter:excl',
              '#PBS -A ec043',
              f'export PATH=$PATH:{omp_path}',
              f'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{omp_lib_path}',
              f'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:{lib_path}',
              'export TMPDIR=/lustre/home/ec043/ball4935/tmp',
              'cd $PBS_O_WORKDIR',
              sep='\n', file=bash_script)

        for filename in filenames:
            output_filename = filename.replace('.inp', '.out')
            print(f'{orca_path} {filename} > {output_filename} &', file=bash_script)

        print('wait', file=bash_script)
        for folder_name in folder_names:
            if len(folder_name) > 0:
                print(f'rm {folder_name}/*.gbw {folder_name}/*property.txt {folder_name}/*.prop', file=bash_script)
            else:
                print('rm *.gbw *property.txt *.prop', file=bash_script)

    return sh_filename


if __name__ == '__main__':

    args = get_args()
    names_and_cores = get_decomposition(file_names=args.filenames)
    for i, (names, cores) in enumerate(names_and_cores):
        script_filename = print_bash_script(filenames=names, n_cores=cores, sh_filename=f'batch{i}.sh')
        os.system(f'qsub {script_filename}')
